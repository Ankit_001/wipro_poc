//
//  Constant.swift
//  WiproPoc
//
//  Created by Ankit Gupta on 17/04/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit

class Constant: NSObject {
    struct KConstant {
        static let url = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
        static let cell_identifier = "ItemTableviewCellTableViewCell"
        static let indicator_title = "Loading"
    }
}
