//
//  ItemTableviewCellTableViewCell.swift
//  WiproPoc
//
//  Created by Ankit Gupta on 16/04/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import SDWebImage

class ItemTableviewCellTableViewCell: UITableViewCell {
    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet weak var imgView : UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    func populateData(dict_item : [String:Any]){
        let title = dict_item["title"] as? String
        self.lblName.text = title ?? "N/A"
        let description = dict_item["description"] as? String
        self.lblDesc.text = description ?? "N/A"
        let image = dict_item["imageHref"] as? String
        imgView.sd_setImage(with: URL(string: image ?? "Placeholder"), placeholderImage: UIImage(named: "Placeholder"))
    }
}
    

