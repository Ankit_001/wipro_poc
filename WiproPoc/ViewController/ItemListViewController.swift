//
//  ItemListViewController.swift
//  WiproPoc
//
//  Created by Ankit Gupta on 16/04/20.
//  Copyright © 2020 Ankit Gupta. All rights reserved.
//

import UIKit
import MBProgressHUD

class ItemListViewController: UIViewController {
    @IBOutlet weak var tblViewItem : UITableView!
    var array_detail : [Any] = []
    private let refreshControl = UIRefreshControl()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        // Do any additional setup after loading the view.
        
        self.tblViewItem.register(UINib(nibName: Constant.KConstant.cell_identifier, bundle: nil), forCellReuseIdentifier: Constant.KConstant.cell_identifier)
        
        self.tblViewItem.rowHeight = UITableView.automaticDimension
        self.tblViewItem.estimatedRowHeight = 44
        
        self.tblViewItem.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(refreshItemData(_:)), for: .valueChanged)
        
        self.callApi()
        
    }
    @objc private func refreshItemData(_ sender: Any) {
        // Fetch item Data
        self.callApi()
        self.refreshControl.endRefreshing()
        
    }
    func callApi(){
        DispatchQueue.main.async {
            self.showIndicator(withTitle: Constant.KConstant.indicator_title, and: "")
        }
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        let url = URL(string: Constant.KConstant.url)!
        let task = session.dataTask(with: url) { data, response, error in

            // if there is no error for this  response
            
            DispatchQueue.main.async {
                self.hideIndicator()
            }
            guard error == nil else {
                print ("error: \(error!)")
                return
            }
            // if there is some data  from this  response
            guard let content = data else {
                print("there is no data")
                return
            }
            let str_data = String(data: content, encoding: .isoLatin1)?.data(using: .utf8)
            // serialise the data  into Dictionary
            guard let json = (try? JSONSerialization.jsonObject(with: str_data!, options: JSONSerialization.ReadingOptions.mutableContainers)) as? [String: Any] else {
                print("Not containing JSON")
                return
            }
            print("json response dictionary is \n \(json)")
            
            if let array  = json["rows"] as? [Any]{
                self.array_detail = array
                print(self.array_detail)
            }
            
            // update UI using the response here
            DispatchQueue.main.async {
                self.navigationItem.title = json["title"] as? String
                let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
                self.navigationController?.navigationBar.titleTextAttributes = textAttributes
                self.tblViewItem.reloadData()
            }
        }

        // execute the HTTP request
        task.resume()
    }
}

extension UIViewController {
   func showIndicator(withTitle title: String, and Description:String) {
      let Indicator = MBProgressHUD.showAdded(to: self.view, animated: true)
      Indicator.label.text = title
      Indicator.isUserInteractionEnabled = false
      Indicator.detailsLabel.text = Description
      Indicator.show(animated: true)
   }
   func hideIndicator() {
      MBProgressHUD.hide(for: self.view, animated: true)
   }
}

extension ItemListViewController : UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.array_detail.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: Constant.KConstant.cell_identifier, for: indexPath) as? ItemTableviewCellTableViewCell{
            if let dict_item = self.array_detail[indexPath.row] as? [String:Any]{
                cell.populateData(dict_item: dict_item)
            }
            return cell
        }else{
            return UITableViewCell()
        }
    }

}
